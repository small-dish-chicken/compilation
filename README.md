# 编译原理 自底向上语法分析
#### Ashley Wang

## 简要说明
#####
这是一个编译原理自底向上进行语法分析的语法分析器，可以根据用户输入的文法自动的生成状态转换表，并根据生成的状态转换表进行语法分析
（词法分析写的较为敷衍）

## 文件说明
![](upload/folder.png)
1. analysis：词法分析和文法分析脚本文件夹
2. entity：实体文件夹，存放python类
3. output：输出文件夹，用于存放输出文件
4. resource：输入文件夹，用于存放输入文件
5. stateTable：状态转换表生成文件夹，存放状态转换表生成相关脚本
6. test：测试样例，resource.txt->setGrammar.txt，test.txt->test.grammar
7. upload: 上传git截图资源

## 功能及使用说明
### 准备工作
![](upload/setGrammar.png)
![](upload/setTest.png)
将需要进行语法分析的代码的文法按照一定格式输入resource/setGrammar.txt文档中，将需要进行分析的代码输入到resource/test.grammar文件中

### 生成状态转换表
直接运行main.py脚本即可在output/stateTable.csv文件中生成状态转换表
![](upload/moveTable.png)

### 进行词法分析
![](upload/lexicalAnalysis.png)
运行tokens.py脚本即可在output/lexicalAnalysis.txt文件中生成对test.grammar进行词法分析的四元组 （类型，名称，值，位置）


### 进行语法分析
![](upload/syntacticAnalysis.png)
运行syntax.py脚本即可在output/syntacticAnalysis.txt文件中生成对test.grammar语法分析的过程


## 工作原理
### 生成状态转换表
#### 计算First集
1. isEpsilon：递归的计算是否文法左部非终结符是否可以产生串 
2. firstFirst：初步进行计算每一个非终结符的First集，得到每个非终结符的可能含有非终结符的first集
3. firstSecond：二次计算非终结符的First集，合并ufSet和fSet的First集
#### 计算Follow集
1. followFirst：初步计算初步进行计算每一个非终结符的follow集，得到每个非终结符的可能含有非终结符的follow集（需要借助上面计算出的first集）
2. followSecond：依据计算first集相同的原理计算最终的follow集
#### 生成状态和规约项
1. 根据grammar类_pos属性（使用!表示），closure文法，生成有穷状态机的状态
2. 依据!是否到达文法末尾判断状态是否可以进行规约，从而对改状态应该进行移入操作还是规约操作进行判断
3. 将生成状态和规约项toString，输入到文件中
#### 自动生成状态转换表
1. 依据status类的方法getDire【获得可以前进的方向】和forward【前进到新的位置】，构建自动状态机（虚拟）
2. 将状态转换表写入moveTable.csv文件中

### 词法分析
设置token单词类，通过getToken函数每次读取一个单词，与设置在tool中的保留字符进行对比，从而进行词法分析，并将生成的词法分析四元组输出到lexicalAnalysis文件中

### 语法分析
进行语法分析使用到的核心函数：
1. readGram：对setGrammar文件进行读取，获取序号-文法的map
2. readMachine：读取moveTable，确定语法分析过程中下一步跳转
3. 最后将生成的语法分析结果输出到syntacticAnalysis文件中

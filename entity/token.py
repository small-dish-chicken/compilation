#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Time    : 2023/5/18 11:38
# @Author  : SDU Wang Yishuo
# @FileName: token.py.py
# @Software: PyCharm

class Token:
    # typ 类型有三种，关键字 标识符，数字
    # iden 存储符号
    # val 存储值
    def __init__(self, typ, iden, val):
        self.typ = typ
        self.iden = iden
        self.val = val
term = [':=', 'begin', 'call', 'integer', 'do', 'end', 'id', 'if', 'mn', 'ood',
        'procedure', 're', 'td', 'then', 'un', 'float', 'while', 'var']

# 生成状态转换表时的表头
title = ['integer', 'float', 'procedure', 'begin', 'end', 'ood', 'if', 'then', ''
         'while', 'do', 'call', 'var', '#', ':=', '=', '(', ')', ';', ',', '.', 'id', 'un', 're', 'mn',
         'td']

# 关键字整型
INT_SYM = 0  # integer
FLO_SYM = 1  # float
PRO_SYM = 2  # procedure
BEG_SYM = 3  # begin
END_SYM = 4  # end
OOD_SYM = 5  # ood
IF_SYM = 6  # if
THE_SYM = 7  # then
WHI_SYM = 8  # while
DO_SYM = 9  # do
CAL_SYM = 11  # call
VAR_SYM = 12  # var

# 符号整型
GIV_SYM = 20  # :=
EQ_SYM = 21  # =
LB_SYM = 22  # (
RB_SYM = 23  # )
SEM_SYM = 24  # ;
COM_SYM = 25  # ,
DOT_SYM = 26  # .

# 自定义终结符整型
ID_SYM = 30  # 标识符
UN_SYM = 31  # 数字
RE_SYM = 32  # > < >= <= ==
MN_SYM = 33  # + -
TD_SYM = 34  # * /
OVE_SYM = 35  # #

# 类型序号-符号映射表，用于对应下推自动机
symTranMap = {0: 'integer', 1: 'float', 2: 'procedure', 3: 'begin', 4: 'end', 5: 'ood', 6: 'if', 7: 'then', 8: 'while',
              9: 'do', 11: 'call', 12: 'var', 20: ':=', 21: '=', 22: '(', 23: ')', 24: ';', 25: ',', 26: '.', 30: 'id',
              31: 'un', 32: 're', 33: 'mn', 34: 'td', 35: '#'}

opeSet = ':=', '=', '(', ')', ';', ',', '.', 'id', 'un', 're', 'mn', 'td'
# 所有关键字
keywordSet = {'integer', 'float', 'procedure', 'begin', 'end', 'ood', 'if', 'then', 'while', 'do', 'call', 'var'}
# 关键字-类型映射
keywordSymMap = {'integer': INT_SYM, 'float': FLO_SYM, 'procedure': PRO_SYM, 'begin': BEG_SYM, 'end': END_SYM,
                 'ood': OOD_SYM, 'if': IF_SYM,
                 'then': THE_SYM, 'while': WHI_SYM, 'do': DO_SYM, 'call': CAL_SYM, 'var': VAR_SYM}
# 符号-类型映射
symSymMap = {'+': MN_SYM, '-': MN_SYM, '*': TD_SYM, '/': TD_SYM, '(': LB_SYM, ')': RB_SYM, ';': SEM_SYM, ',': COM_SYM,
             '.': DOT_SYM}


# 把分析栈中的符号映射为对应的终结符号
# 它的输入参数是一个字符串 right，代表一个待映射的符号
# 通过遍历全局变量 term 来找到符合要求的终结符号，并将其返回
# 如果遍历结束后仍未找到符合要求的终结符号，则直接返回该符号的第一个字符
def getTerm(right):
    for t in term:
        if len(t) < 3:
            if t == 're':
                # print(right)
                if len(right) >= 3 and right[2] == 'a':
                    return 'read'
            if right[0] == t[0] and right[1] == t[1]:
                return t
        else:
            if right[0] == t[0] and right[1] == t[1] and right[2] == t[2]:
                return t
    return right[0]


if __name__ == '__main__':
    print(getTerm('<ddd>end'))
